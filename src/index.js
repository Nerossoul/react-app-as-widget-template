import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'

const el = document.getElementById('widget')
ReactDOM.render(<App />, el)
